// JavaScript for main HTML elements and navigation sidebar
//
// normally these routines do not need editing or customization,
// as long as the HTML elements and classes in index.html
// are not renamed.
//
// this code is executed once index.html has been loaded.
//
// Jeffrey Biesiadecki, 1/6/2018

// initialize main elements and the navigation sidebar
{
  let elementList;
  let element;

  // create action function to open the nav sidebar
  let nav_open = function() {
    // show the nav_sidebar and close button, hide the open button
    document.getElementById('nav_sidebar').style.display = 'block';
    document.getElementById('nav_open_button').style.display = 'none';
    document.getElementById('nav_close_button').style.display = 'block';

    // fit the content to the right of the sidebar
    document.getElementById('main').style.left =
      document.getElementById('nav_sidebar').style.width;
  }

  // create action function to close the nav sidebar
  let nav_close = function() {
    // hide the nav_sidebar and close button, show the open button
    document.getElementById('nav_sidebar').style.display = 'none';
    document.getElementById('nav_open_button').style.display = 'block';
    document.getElementById('nav_close_button').style.display = 'none';

    // fit the content to the edge of the screen
    document.getElementById('main').style.left = '0';
  }

  // create action function to select active sidebar button
  let nav_activate = function() {
    let clicked_id = this.id;
    let elementList;

    // activate the clicked button, and make sure all others are inactive
    elementList = document.getElementsByClassName('nav-sidebar-button');
    for (let i = 0; i < elementList.length; i++) {
      let element = elementList[i];

      if (element.id === clicked_id) {
        element.classList.add('nav-active');
      } else {
        element.classList.remove('nav-active');
      }
    }

    // also, make sure all accordions are marked as inactive
    elementList = document.getElementsByClassName('nav-sidebar-accordion');
    for (let i = 0; i < elementList.length; i++) {
      let element = elementList[i];

      element.classList.remove('nav-active');
    }
  }

  // create action function to open/close an accordion list
  // in the nav sidebar
  let nav_accordion = function() {
    let clicked_id = this.id;
    let element_div = document.getElementById(clicked_id + '_div');
    let element_caret = document.getElementById(clicked_id + '_caret');

    if (element_div.classList.contains('w3-hide')) {
      // accordion was hidden, so now display
      element_div.classList.remove('w3-hide');
      element_div.classList.add('w3-show');

      // change caret symbol to down-pointing small triangle
      element_caret.innerHTML = '&#9662;';

      // make sure accordion button itself is inactive
      this.classList.remove('nav-active');
    } else {
      // accordion was displayed, so now hide
      element_div.classList.add('w3-hide');
      element_div.classList.remove('w3-show');

      // change caret symbol to right-pointing small triangle
      element_caret.innerHTML = '&#9656;';

      // if a button in this accordion is active,
      // then also mark the accordion as active
      // (so it is easy to tell which accordion was last selected)
      let elementList =
        element_div.getElementsByClassName('nav-sidebar-button');
      for (let i = 0; i < elementList.length; i++) {
        let element = elementList[i];

        if (element.classList.contains('nav-active')) {
          this.classList.add('nav-active');
          break;
        }
      }
    }
  }

  // assign nav sidebar open/close buttons
  document.getElementById('nav_close_button').onclick = nav_close;
  element = document.getElementById('nav_open_button');
  element.onclick = nav_open;
  element.onclick(); // and open navigation sidebar

  // assign nav sidebar buttons
  elementList = document.getElementsByClassName('nav-sidebar-button');
  for (let i = 0; i < elementList.length; i++) {
    let element = elementList[i];

    element.onclick = nav_activate;
  }

  // assign nav sidebar accordions
  elementList = document.getElementsByClassName('nav-sidebar-accordion');
  for (let i = 0; i < elementList.length; i++) {
    let element = elementList[i];

    element.onclick = nav_accordion;
    element.onclick(); // and close accordion
  }

  // activate initial button
  elementList = document.getElementsByClassName('nav-sidebar-button');
  element = elementList[0];
  element.classList.add('nav-active');
  document.getElementById('main_iframe').src = element.href;
}
